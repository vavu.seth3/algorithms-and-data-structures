# Depth First Search (DFS)

DFS is a "graph traversal" algorithm, formally graph traversal is just when we start at a single vertex and find a way to get to every other vertex in the graph via any path. We can in fact arrive at a simpler description of graph traversal algorithms and define them as any algorithm that visits EVERY node in the graph. What usually differentiates graph traversal algorithms however is the order in which they visit vertices.

DFS is a relatively simple algorithm so my implementation will remain simple and so will my brief explanation of it. Consider the following graph $G$:

![](../resources/graph.png)

<br/> <br/>

## Algorithm
The intuition behind the algorithm is that we start at a node and we visit all neighbors of the node. We then continue doing this until we reach a node where there are no more nodes that we can visit and haven't seen before. Once we reach this "dead-end" we go to the furthest-back node that we haven't seen and restart the process. There are two implementations in Python provided below:

**Recursive Implementation**
A simple pseudo-python implementation of DFS is as follows:
```python
def DFS(n: node, seen_nodes = {}) -> list:
    traversal = [n]
    
    for u in Graph.neighbors(n):
        if u is not in seen_nodes:
            seen_nodes[u] = true
            traversal += DFS(u)
    return traversal
```

**Iterative implementation**
The above implementation is based on recursion, the implementation to follow is iterative:

```python
def DFS(n: node) -> list:
    stack = [n]
    traversal = []

    while len(stack) is not 0:
        u = stack.pop()
        traversal.append(u)

        if u in traversal: continue

        for v in Graph.neighbors(u):
            if v not in traversal: stack.push(v)
    return traversal
```
<br/> <br/>

## Complexity

Assuming the "seen?" operation takes constant time then the algorithm visits each node uniquely once. Now consider a node $u$ with neighbors $\{a, b, c, \cdots\}$. There are two possible options when recursively calling DFS on these neighbor nodes, if a has been seen then we do not recursively call DFS on a, if it has then we do, thus we can conclude that we visit each edge $(u, a)$ uniquely once and as such the overall complexity of DFS is
$O(|V| + |E|)$.
<br/><br/>

## Applications of DFS

DFS has some really cool and important applications. Some of these are outlined below:
 - ##### Edge classification
    Within a graph there are different types of edges, DFS provides us with an efficient method of classifying all edges within a graph. We are going to classify edges by the following categories: <br/>

     - **Back Edge**: An edge $(v_{0}, v_{1})$ where $v_{0}$ was an "ancestor" of $v_{1}$. This is rather easy to determine using DFS, when we reach a node $v_{1}$ if $v_{1}$ has an edge to $v_{0}$ and it has been "seen before" then $(v_{1}, v_{0})$ is a back edge.

     - **Forward Edges**: A forward edge $(v_{0}, v_{1})$ is an edge where $v_{1}$ is a descendant of $v_{0}$. Once again this is rather simple and just involves determining if the nodes have been seen before.

     - **Cross Edge**: A cross edge is an edge $v_{0} \to v_{1}$ where each of these are nodes in two different "regions". 

    A visual depiction of the different types of edges is as follows:
    ![](../resources/edge_classification.png)


 - ##### Cycle detection
    We can utilise DFS for cycle detection, it simply involves finding a back-edge. If we find a single back-edge then we can conclude that a cycle exists in $G$.

 - ##### Topological Sort
    If we consider a DAG which represents some "compilation process". Some nodes come before other nodes and as such must be "compiled" before the other nodes. We want to figure out an order that allows us to compile the files and not violate and dependencies. This is in essence just the printing order of DFS.


