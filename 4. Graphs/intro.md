# Graphs

Graphs are essentially a description of some sort of "relationship" between objects of a specific set $S$, thus a graph G follows the following property:
$G \subset S \times S$.

Graphs are important because they essentially represent the relationship between objects, before we start studying algorithms we can perform on graphs it may be important to first cover some terminology and applications of graphs... Formally we define graphs as follows:

$G = (V, E)$

Where $V$ is the set of vertices and edges $E$ where each edge $(u, v)$ is a connection between the vertices. $u, v \in V$.


We will study some elementary definitions and properties of graphs but for starters consider the following graph $G$:

![](../resources/graph.png)

#### Definitions / Terminology

 - **Nodes**: Since graphs are essentially a relationship over some set S, we define a node as an object within S, aka visually a node is simply a circle with a value in it.
 - **Edges**: An edge is a depiction of the "relationship" between two nodes, visually an edge is just a line between two nodes.
- **Neighbors**: Two vertices $u, v$ are neighbors if there if there is an edge $(u, v) \in E$ that connects them together. Eg. $neighbors(B) = \{D, E, A \}$
- **Degree**: $degree(v)$ is equal to the number of neighbors connected to $v$. 
Formally: $degree(v) = |\{u: (u, v) \in E\}|$
- **Paths**: A path is simply a sequence of vertices connected by edges. 
    - **Path Length**: This is simply the number of edges in a path. 
    - **Cycle**: A cycle is simply a path that starts and ends on the same vertex
    - **Connectivity**: Two vertices are connected if a path exists between them such that the two vertices are the end-points of the path. A graph $G$ is considered connected when all vertices in the path are connected. 
    - **Connected Component**: This is a subset of vertices $V_{i} \subset V$ that is connected.

#### Types of Graphs

 - **Undirected Graphs**: A graph $G$ is undirected if BOTH $(u, v) \in E$ and $(v, u) \in E$.
 - **Directed Graphs**: These are simply graphs that aren't undirected ;). 
 - **Acyclic/Cyclic Graphs**: An Acyclic graph is a directed graph with NO cycles while a Cyclic graph is a directed graph with cycles.
 - **Weighted Graphs**: These are graphs where each edge has a payload associated with it, they are useful for modelling networks and traffic flow.
 - **Trees**: These are a special type of graphs that are
    1. Connected and Acyclic
    2. Removing edges disconnects the graph
    3. Adding edges creates a cycle




Now that we have a description of the mathematical characteristics of a graph, we need to figure out how to implement the graph ADT computationally, there are a few options as follows:
<br/><br/><br/>


## ADT implementations

 - #### Adjacency matrix
    An adjacency matrix is essentially a representation of all the edges in a graph, if we consider the entry $(i, j)$ we say it is 1 if an edge between the two exists and 0 if otherwise. Formally we define the adjacency matrix as follows:

    $A_{ij} = \begin{cases}
        \text{1 if } (i, j) \in E \\
        \text{0 if otherwise}
    \end{cases}$ 

 - #### Edge Set
    An edge set is simply just the set of all edges in the graph. Intuitively we are just implementing a graph directly based of its formal definition (not very efficient).

 - #### Adjacency List
    This is just a mapping from each vertex to all its neighbors, this can be achieved with a dictionary in python: 
    ```python
        x = {
            'a': {'b', 'c', 'd'},
            'b': {'c', 'a'},
            'c': {'b', 'd'}
            'd': {'d'}
        }
    ```
    This allows us to determine all the neighbors of a vertex in constant time.