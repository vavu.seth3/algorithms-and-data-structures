# Hashtables + Direct Access Tables

The idea behind implementing a map with a direct access table is that we essentially store all our data in an array, the key of each item represents the index of the value we are storing and the value at that index is the value of the item.

While this is a fine method it provides the fundamental issue of a limited use-case, keys must be unsigned 32-bit integers and as such the implementation of the ADT does not generalise. 

This motivates the idea of a hashtable, hastables are essentially direct access tables that utilise a special "hash function" to map the universal set of all keys to a unique integer. 
<br/>

#### Hash functions - P1
Before deep-diving into hash-tables I would first like to concentrate on the idea of a hash function. A hash-function defined as $h(x)$ is any function:

$H: \mathrm{U} \to \{0, 1, 2, \cdots, n - 1\}$

Where $n$ is the size of the "direct access table". Now an important thing to note here is that by the pigeon-hole principle, since the universal set is infinite and the integer set is finite we will fundamentally end up with **collisions** where multiple distinct key map to the same integer. As such any effective implementation that utilises hash-functions must be able to deal with collisions effectively.  Another thing to notice is that the size of the direct access table is fixed, in an idealised world it may not always be fixed in size and an effective hash function should be able to deal with this effectively. 
<br/><br/>

## Hash tables
Hash tables as discussed before are basically just direct access tables that utilise hash functions to map keys to array entries. We must however, find an effective method of combating collisions and the scaling of tables, below I present multiple different collision response strategies, outline their implementation and provide an analysis.
 - #### Chaining
    The idea behind chaining is that each bucket in the array is a pointer to the head of a linked list, whenever a key maps to an index we simply just insert it into the linked list. This deals with collisions quite obviously as it just results in an ever-expanding linked list. An interesting thing to note is that we can in fact use any data structure as our "chain", we are not restricted to a linked list. From here, implementation of a Dictionary should be rather trivial but a pseudo-python implementation of various operations is provided below:
    ###### Insertion
    Insertion requires locating the index of the array and inserting the value into the data structure being used for chaining. The pseudo-python implementation is as follows:
    ```python
    def insert(self, key, val):
        x = hash(key)
        self.DAT[x].append((key, val))
    ```

    ###### Deletion
    Deletion is just as simple as insertion, locate the bucket and delete it from the "chain":
    ```python
    def delete(self, key, val):
        x = hash(key)
        self.DAT[x].delete((key, val))
    ```

    ###### Searching
    Finally searching is defined as:
    ```python
    def search(self, key) -> val:
        x = hash(key)
        return self.DAT[x].search(key)
    ```

    ##### Worst Case Analysis
    In the worst case all keys in the universal set map to a single index, as such the worst case for all the algorithms become the worst case complexities for the data structures being used as chains/buckets. If a linked list is used then this is $O(n)$ for searching and deletion as well as $O(1)$ for insertion (insert at the head).

    ##### Average case Analysis
    Before our analysis, we define $\alpha = \frac{n}{m}$ where $n$ is the amount of entries and $m$ is the size of the table as the average number of elements stored in our chains, note that we will be using a linked list as our chain. We denote $\alpha$ as the load factor.
    <br/>

    For $j = 0, 1, \cdots, m - 1$ we denote the length of the chained list as $T[j]$ as $n_{j}$ such that $n = n_{0} + n_{1} + n_{2} + \cdot + n_{m-1}$ and the expected value of $n_{j}$ is $E[n_{j}] = \alpha$ 
    <br/>

    1. **Searching**
    Analysis of chaining will occur using a simple linked list as our chain. Note that when we insert an element into our chain, we insert it at the front of the chain and by extension, the average complexity of a successful search depends on the number of items "added" to the item's bucket **AFTER** the item was added. We define the indicated random variable: <br/>
    $X_{ij} = I\{h(k_{i}) = h(k_{j})\}$.<br/>
    Where $k_{i}$ is the key of the ith inserted element and $k_{j}$ is the key of the jth inserted element. As a precursor to the analysis, I'd like to define a random variable $X_{n}$ as the average number of comparisons for $n$ randomly inputted variables, hence we define $X_{n}$ as:
    $\begin{aligned}
        X_{n} = \frac{1}{n} \sum_{i = 1}^{n}\left(1 + \sum^{n}_{j = i + 1} X_{ij}\right)
    \end{aligned}$
    And by extension the average case complexity is $E[X_{n}]$, from this the analysis follows quite simply: <br/>
    $\begin{aligned}
        T_{n} = E\left[\frac{1}{n} \sum_{i = 1}^{n}\left(1 + \sum^{n}_{j = i + 1} X_{ij}\right) \right]\\\\ \text{By linearity of expectation: } \\ = \frac{1}{n} \sum_{i = 1}^{n}\left(1 + \sum^{n}_{j = i + 1} E[X_{ij}]\right)\\=\frac{1}{n} \sum_{i = 1}^{n}\left(1 + \sum^{n}_{j = i + 1} \frac{1}{m}\right)\\=\sum_{i = 1}^{n}\left(\frac{1}{n} + \frac{1}{nm}\sum^{n}_{j = i + 1} 1\right)\\=1 + \frac{1}{nm}\sum^{n}_{i = 1} \left(n-i\right)\\= 1 + \frac{1}{nm}\left(n^{2} - \frac{n(n + 1)}{2}\right) \\= 1 + \frac{\alpha}{n} - \frac{\alpha}{2n}\\= O(1 + \alpha)
    \end{aligned}$
    An important thing to note firstly is that in the limit where $n \to m$, the chaining results in an average linear time, furthermore if we use a good hash function, the uniform hashing argument still holds quite well and we can assume on average, linear access time. 
    <br/>


 - #### Table Doubling
    Note how the average case complexities for all are a function of the load factor for the table, since $\alpha = \frac{n}{m}$, if n grows too large then the load factor grows too and searching is no longer constant. As such we must devise a method to keep the load factor rather low. We have two adjustable options, $n$ or $m$, since we cannot change $n$, we must change $m$. The method presented to do this is known as **table doubling** and aims to maintain $m = O(n)$. <br/>
    Table doubling in essence doubles the value of $m$ whenever we realise n is getting too close to $m$. Note that whenever we double $m$ we need to completely rebuild the hash table from scratch and insert everything into it. We shall analyse the computational complexity of the table doubling algorithm as follows: 
    <br/> Define $T_{m}$ as the complexity of arriving at a table of size $m$ (we assume $m$ is a power of two but our argument generalises), starting at a table of size $1$ via table doubling. <br/>
    $\begin{aligned}
        T_{m} = 1 + 2 + 4 + 8 + \cdots + m \\=\sum_{k = 0}^{\log_{2}{m}} 2^{k} = 2 \cdot 2^{log_{2}{m}} + 1\\\implies T_{n} = 2m + 1 = O(m)
    \end{aligned}$ 
    <br/>

    And hence table doubling has a linear complexity. Now the question becomes how exactly we implement table doubling in our hash table. Well the obvious solution (and the one we are going with) is to perform a table double whenever $n = m$. This however poses some questions, as since table doubling is a linear time algorithm, does insertion then become $O(m)$ on average too? Well, its not fair to say so, so I would like to introduce a new type of analysis known as "amortized" analysis where we average the cost of a large "one-off" computation over every execution of the algorithm, sort of like "paying rent" or "saving". 
    <br/>
    ###### Amortized Analysis
    We define the amortized cost of an algorithm as $T(n)$ if k operations cost $\leq k \cdot T(n)$. Non-rigorously, amortized cost simply refers to a sort of "average" cost over all operations, as its unfair to "duc" an algorithm for making one expensive computation that advantages it in the long run. We claim that the average case complexity of insertion into a hash table is $O(1)$ amortized, the proof is as follows: <br/>
    $\begin{aligned}
        \text{k operations have the following complexity: } O(n) + \sum^{k}_{n = 1} O(1) = O(k) \\ T_{k} = O(1) \implies k \cdot T{k} = O(k) \\ O(k) \leq O(k)\\ \therefore \text{Amortized: } T_{k} = O(1)
    \end{aligned}$ 
    <br/>

<br/><br/>

#### Hash functions - P2
After studying hash tables, its rather evident that we require an effective hash function that is capable of maintaining our "uniform hash" assumption, that is: the hash function has to hash to each bucket with an equal probability, this section is dedicated to a description and analysis of such hash functions. 
 1. **The division method:** $h(k) = k \text{ mod } m$
 2. **The multiplication method:** $h(k) = m \cdot \left(kA - floor\left(kA\right)\right)$
 3. **Universal Hashing:** Now this is an interesting method that I think is worth some more discussion but put simply: $h(k) = [(ak + b) \text{ mod } p] \text{ mod } m$. <br/>
 To motivate universal hashing I'd like to use the same argument I laid forth for randomised quicksort, assume that a malicious user has information regarding the nature of our hash function, that malicious user can then guarantee worst-case behavior by providing keys that all hash to the same bucket, this however, can be avoided if we randomise our hash function and remove all information from the user, as such this is another elegant example of a **randomised algorithm :)**. <br/>
 Now onto the function... We define a finite family of hash functions $F$, at runtime we randomly pick a hash function from this family. As such each function has a $\frac{1}{|F|}$ chance of being selected. Now since randomisation occurs at runtime, we can assume that the user cannot change their inputted data once a hash function has been chosen. As such the probability of a collision occurring between two inputs becomes: <br/>
$\begin{aligned}
    \frac{|\left\{h: h(x) = h(y)\right\}|}{|F|}       
\end{aligned}$ <br/> 
The trick now is to actually find our family of hash functions, we will assume that we are hashing integers. I'm not going to actually go through a derivation of our family, I'll just provide them to you as follows:<br/>
$F = \left\{h_{ab}: 0 < a < p\text{  ,  } 0 \leq b < p\right\}$ 
$h(x) = [(ax + b) \text{ mod } p] \text{ mod } m$<br/>
Where p is any arbitrary prime number bigger than $m$. Now I won't provide the proof here as it requires some level of familiarity with modular arithmetic and number theory but take it as a given that using this scheme, the probability of a collision is at most $\frac{2}{m}$. As such this scheme abides by the "uniform hashing" assumption that was the cornerstone of all our analysis. 