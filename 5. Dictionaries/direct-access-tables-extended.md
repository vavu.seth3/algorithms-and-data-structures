# Extended Direct Access tables

The purpose of this section is the build upon the ideas used in our hash table implementation of a dictionary ADT and provide some alternative implementations. We will explore alternative methods of dealing with collisions outside of chaining. 

### Open Addressing
The idea behind open addressing is that the hash table contains all elements of the table within it and we do not use an external data structure for chaining. When searching for an element we systematically examine table slots until we fine the value we are looking for. A consequence of this is that the load factor $\alpha$ never exceeds 1. 

The question then becomes how we achieve this... Well with open addressing, instead of following and chasing pointers we instead compute a sequence of table entries that we need to search for when searching for a value, this results in a searching complexity that isn't exactly linear. This process of computing slots to be checked and checking them is known as "probing" and we say that we successively "probe" as hashtable to find what we are looking for. To achieve this, we define a new type of hash function: <br/>
$H: \mathrm{U} \times \{0, 1, \cdots, m- 1\} \to \{0, 1, \cdots, m - 1\}$<br/>
Where the number represents the "probe number" or our "current position" and the output represents the position we need to move to. With open addressing we can then define a probe sequence: $\left<h(k, 0), h(k, 1), \cdots, h(k, m - 1)\right>$.
Below are some elementary methods that we can use to define our new "probing hash function"

1. **Linear Probing:** Given an auxillary hash function $h(x)^{*}$ we define our new "probing function" as: <br/>
$h(x, i) = (h(x)^{*} + i) \text{ mod } m$. <br/>
This method has issues with clustering as values will tend to "group together" resulting in longer access times.
2. **Quadratic Probing:** Like linear probing except: <br/>
$h(x, i) = (h(x)^{*} + c_{1}i + c_{2}i^{2}) \text{ mod } m$<br/>
This method however tends to suffer from issues of "secondary" clustering which while not as bad as linear probing, it is very annoying. 
3. **Double Hashing:** Given two auxillary hash functions: $h_{1}(x)$ and $h_{2}(x)$, we define our hash function as follows: <br/>
$h(x, i) = (h_{1}(x)+ ih_{2}(x)) \text{ mod } m$<br/>
This method is much better than the previous methods and has virtually no clustering issues. We utilise this method with open addressing. <br/>

#### Analysis
While this new technique is cool, we can't be entirely sure that it performs that much better than a chained hash table, for starters, it doesn't support load factors over 1. Before beginning any form of actual analysis I would like to compute the expected number of probes required to perform an unsuccessful search of the table, that is, how many probes do we require to realise that our table doesn't have our data? I assume uniform hashing to make the analysis slightly easier. 

We define the random variable $X$ as the number of probes performed in a search of the table, X can take any value from the set $\mathrm{N}$. To compute the expected value of $X$ we employ a little trick regarding random variables over the natural numbers but we must first compute the probability that $P(X \geq i)$. The event that $X \geq i$ will only occur if we face a collision on our 1st, 2nd, 3rd, ..., i-1th probe, as such we can use the uniform hashing assumption to compute the probability of this even occurring. <br/>
$\begin{aligned}
    P(X \geq i) = \frac{n}{m} \cdot \frac{n - 1}{m - 1} \cdot \frac{n - 2}{m - 2} \cdots \frac{n - i + 2}{m - i + 2} \\ \leq \left(\frac{n}{m}\right)^{i - 1} \\= \alpha^{i - 1}
\end{aligned}$<br/>
Thus $P(X \geq i) = O\left(\alpha^{i - 1}\right)$.

Now we can compute the expected number of probes... <br/>
$\begin{aligned}
    E[X] = \sum^{\infty}_{n = 1} n \cdot P(X = n) = \sum^{\infty}_{n = 1} n \cdot \left(P(X \geq i) - P(X \geq i + 1)\right) \\= \sum^{\infty}_{n = 1} P(X \geq n) \\\leq\sum^{\infty}_{n = 1} \alpha^{n - 1} = \sum^{\infty}_{n = 0} \alpha^{n} \\= \frac{1}{1 - \alpha} \\\\ \therefore E[X] = O\left(\frac{1}{1 - \alpha}\right)
\end{aligned}$<br/>
This is a cool result and basically tells us that the expected number of probes that we make depends entirely on the current load factor of the table, as such, if we keep the load factor low, we can limit the average number of probes. We can now analyse some basic operations.
###### Insertion
Insertion implies that all the probes that we've made up to a specific point have failed, as such on average we can expect the average number of required probes for insertion to be equivalent to a failed search, thus: <br/>
 $\begin{aligned}T_{n} = O\left(\frac{1}{1 - \alpha}\right)\end{aligned}$<br/>

###### Successful Searching
Searching for a key produces the same probe sequence as inserting that key into the table, we can compute the expected number of probes from searching by computing the "average" amount of probes required to insert anything into the table. We define $T_{n}$ as the average amount of "insertion" probes. <br/>
$\begin{aligned}
    T_{n} = \frac{1}{n} \sum^{n - 1}_{i = 0} \frac{1}{1 - \alpha} = \frac{1}{n} \sum^{n - 1}_{i = 0} \frac{m}{m - i} \\\\= \frac{1}{\alpha} \sum^{m}_{k = m-n + 1} \frac{1}{k} \\\\\leq \int_{m - n}^{m} \frac{1}{x} dx \\\\= \frac{1}{\alpha} \ln{\frac{m}{m - n}} = \frac{1}{\alpha}\ln{\frac{1}{1 - \alpha}} \\\\ \therefore T_{n} = O\left(\frac{1}{\alpha} \ln{\frac{1}{1 - \alpha}}\right)
\end{aligned}$<br/>

I think an important thing to note here is that the average case complexities of all operations are heavily dependent on the load factor of the table and as such **WE MUST** try and keep to load factor low to maintain efficient operations. This like with the case of chained tables can be achieved through table doubling. Which results in a $O(1)$ average case complexity across all operations.
<br/>

#### Implementation
Now the way I ordered this topic was rather unusual as my other chapter's tended to follow a strict: intro -> implementation -> analysis rule, but I was just feeling a bit "different today". Below I outline how we implement a dictionary ADT using an open-addressed hash table.
 1. **Searching**
 Searching in an open addressed table like a chained table is a rather simple process, I outline a pseudo-python implementation below...
 ```python
    def search(self, key, i=0):
        probe = hash(key, i)
        if self.DAT[probe] is None: return

        if self.DAT[x].key is not key:
            self.search(key, probe)
        else: return self.DAT[x].val
 ```
 2. **Deletion**
 Now I cover this before insertion as there is one "special" way in which deletion can affect insertion, note that when we try and delete something, IF we just remove it from the table then all future search operations will terminate, this blocks off a huge chunk of any probing sequence that includes the deleted value... to combat this, we simply just mark the cell as "DELETED" instead of outrightly deleting the value, that way our searching operation will work as usual and our insertion operation should fill up that deleted cell when it can.
 ```python
    def delete(self, key, i=0):
        probe = hash(key, i)
        if self.DAT[probe] is None: return

        if self.DAT[x].key is not key:
            self.delete(key, probe)
        else: 
            self.DAT[x].val = DELETED
            return
 ```

 3. **Insertion**
 Insertion just needs to ensure that we override a DELETED value. 
```python
    def insert(self, key, val, i=0):
        probe = hash(key, i)
        if self.DAT[probe] is None or self.DAT[probe] is DELETED:
            self.DAT[probe] = (kev, val)
            return
        else:
            self.insert(key, val, probe)
            return
 ```
