# Dictionaries

Dictionary ADTs are in essence just a generalization of arrays to arbitrary indices, instead of indexing entries based off an integer we can index entries based on an arbitrary value called a "key". This allows us to effectively describe binary relationships between two adjoint or disjoint sets. 

Formally the dictionary ADT is defined by the following operations:
 1. **insert(key, value):** adds a value to a set with a given key
 2. **delete(key):** deletes the value from the dictionary with the associated key
 3. **search(key):** returns the value of the item with a specified key


We can implement a dictionary ADT in a variety of ways and this section aims to explore two of them, specifically: tree-based implementations and hash-table based implementations. 