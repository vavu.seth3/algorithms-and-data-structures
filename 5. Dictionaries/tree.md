# Balanced BSTs and Dictionaries

**Note:** This section assumes some level of familiarity with BSTs; they were introduced in "2. Sorting - Data Structures".

This section doesn't require much discussion as it's rather simple, it just requires comparing keys and using "values" as payloads.
<br/>

### ADT implementation
The ADT is rather simple to implement using a binary search tree, we assume that the tree is balanced throughout the analysis.

1. **Insertion**
Insertion is just a slight modification of regular insertion into a binary search tree, a pseudo-python implementation is provided below:
```python
    def _insert(self, key, val):
        if key > self.key:
            if self.r is not None: self.r._insert(key, val)
            else: self.r = Node(key, payload: val)
        else:
            if self.l is not None: self.l._insert(key, val)
            else: self.l = Node(key, payload: val)   
```

2. **Deletion**
Just like insertion, deletion is the exact same as a normal BST.
```python
    def _delete(self, x: Node):
        if x._isLeaf(): del x
        elif x._isSingleParent():
            replacement_child = x._r if x._l is None else x._l
        else:
            in_order_successor = x._find_successor()
            swap(x, in_order_successor)
            x._delete(in_order_successor)
```

3. **Searching**
I think the algorithm for searching should be rather obvious now after considering the above examples.
<br/>


### Analysis
By the master theorem, all algorithms have a logarithmic complexity $O(\log{n})$.
