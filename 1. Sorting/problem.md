# Sorting #
Sorting is a pivotal problem in computer science (**despite how boring the problem is**). The is a simple one: <br>
>Consider an arbitrary list of numbers $A = [a_{1}, a_{2}, \cdots, a_{n}]$. We want to arrange the list A such that the $n^{th}$ element of the arranged list is less than or equal to the $n^{th} + 1$ element. In other words we want to find an arrangement: $A = [a_{5}, a_{10}, \cdots a_{k}]$ such that the above property is true. If the property is true, we say the list is **sorted**.


There are several "descriptions of steps" (algorithms) for such a task these are: **Selection sort**, **Insertion Sort**, **Binary Insertion Sort**, **Merge sort** and **Heap sort**. A description of each algorithm will be given and an analysis of the algorithm will be carried out on a RAM model of computation. <br/><br/> 


## Selection sort
The intuitive description of the algorithm is as follows: given a list $A$, we consider the list a concatenation of two other lists: $A = [a_{1}] + [a_{2}, a_{3}, \cdots, a_{n}]$. We iterate over the list and swap the element $a_{1}$ with the smallest element in the list, we then consider $[a_{1}]$ sorted and call selection sort on the rest of the list recursively. 
##### Complexity analysis
Given a list $A$ of fixed size $n$, the initial pass over the array access an address in memory $n$ times. The remainder of the list on which insertion sort is called again has a size $n-1$ and hence there are $n-1$ access to memory. Thus for a list of size $n$ the total amount of memory accesses is: 

$\begin{aligned}
    n + (n-1) + (n-2) + \cdots + 1 = \frac{n(n+1)}{2} = O(n^{2})
\end{aligned}$

As such selection sort is a polynomial time algorithm on the RAM machine with a quadratic complexity. A Python implementation is provided.<br/><br/>


## Insertion Sort
Notice how with selection sort, we iterated only over the remainder of the array that was unsorted, the idea behind insertion sort however is to construct a "sorted partition" and slowly add elements into the sorted partition, for example consider: $A = [5, 4, 3, 1, 2]$ initially $A$ is constructed by concatenating two lists: $A = [5] + [4, 3, 1, 2]$ but instead of attempting to sort the $[4, 3, 1, 2]$ and concatenating that to the left hand side, we instead treat $[5]$ as a sorted partition of $A$, we then incrementally insert each member of $[4, 3, 1, 2]$ into $[5]$ and modify $[5]$ to maintain the "sorted" property. More abstractly: $A = A_{partition} + [a_{2}, a_{3}, \cdots a_{n}]$ we then sort $A$ by iterating over $[a_{2}, a_{3}, \cdots, a_{n}]$, inserting $a_{k}$ into $A_{partition}$ in such a way that $A_{partition}$ remains sorted.

The primary component of the sorting algorithm is thus finding a way to insert elements into the partition while maintaining the sorted property. The brute force method is simply iterating over each $i = m, m-1, \cdots, 0$ and if $A_{partition}[i] \leq a_{k} \leq A_{partition}[i+1]$ then we insert $a_{k}$ at the position $i$ such that the property was true. 
##### Complexity analysis
The main loop of the algorithm performs a total "memory access" of $n$ elements, each of these makes a call to the partition insertion algorithm, at most this algorithm will iterate over the size of the partition thus there are **at most** the size of the partition accesses, hence we can conclude that the time complexity is given by: 

$\begin{aligned}
    1 + 2 + \cdots + (n-1) + n = \frac{n(n+1)}{2} = O(n^{2})
\end{aligned}$

This has the same complexity as selection sort but the average case complexity is much faster as most of the time, the insert into partition algorithm does not require a full iteration over the partition, hence this is a substantially better algorithm.<br/><br/>


## Binary Insertion Sort
Binary insertion sort is simply a modification of insertion sort, the partition insertion algorithm utilises binary search to find the desired index as opposed to bubbling to the index, this reduces the time complexity of insertion.
##### Complexity analysis
Note that despite making less comparisons, we still need to "shift" the array along in RAM, under the RAM model of computation this still results in a:

$O(n^{2})$

Complexity, note however that if were instead using a "linked-list" model of a list, the complexity would drop to

$O(nlog_{2}(n))$

as element insertion takes constant time.
<br/><br/>



## Merge Sort
Much like how selection sort was a recursive sorting algorithm, merge sort is also a recursive algorithm, the idea behind merge sort is for a given list $A = A_{1} + A_{2}$ we sort both $A_{1}$ and $A_{2}$ we then merge the two sorted sub-arrays by using a two pointer technique. Note that since we need to sort the two sub-arrays, we can instead just recursively call merge-sort until we reach the degenerate case of a singleton array. Further note that the merge algorithm has a linear complexity. 
##### Complexity Analysis
The total time complexity of the algorithm can be written as a recurrence relation

$\begin{aligned}
    T(n) = 2T\left(\frac{n}{2}\right) + O(n)\\
\end{aligned}$
$\text{The master theorem then states that a solution to the reccurance relation is:}$
$T_{n} = O(n \cdot log_{2}(n))$
<br/><br/>



## Heap Sort
This implementation will be in the next "chapter" as it involves a discussion of heaps (a data structure).<br/><br/>
