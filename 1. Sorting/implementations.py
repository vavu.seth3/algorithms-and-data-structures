# implementations of the various sorting algorithms
import math


# recursive implementation of selection sort
def selection_sort(x: list) -> list:
    if len(x) == 0: return x

    minimum_vals = (x[0], 0)
    for i in range(len(x)):
        if x[i] < minimum_vals[0]: minimum_vals = (x[i], i)
    
    temp = [minimum_vals[0]]
    x.pop(minimum_vals[1])
    return temp + selection_sort(x)




# implementation of insertion sort
def insertion_sort(x: list) -> list:
    # partition insertion algorithm
    def partition_insert(a: list, k) -> list:
        # check intermediate indices
        for i in range(len(a)-1):
            if a[i] <= k <= a[(i+1)]:
                a.insert(i+1, k) 
                return a
        # if it didn't fit there, just fit them into the edges:
        if k <= a[0]:
            a.insert(0, k)
        elif k >= a[-1]:
            a.append(k)
            
        return a


    partition = [x[0]]
    for k in x[1:]:
        partition = partition_insert(partition, k)
    return partition



# implementation of merge sort
def merge_sort(x: list) -> list:
    if len(x) is 1:
        return x
    
    def merge(a: list, b: list) -> list:
        if len(a) == len(b) == 1:
            return a + b if a[0] < b[0] else b + a

        a_cursor = b_cursor = 0
        output_array = []
        while a_cursor != len(a) and b_cursor != len(b):
            if a[a_cursor] <= b[b_cursor] and (a_cursor != len(a) and b_cursor != len(b)):
                output_array.append(a[a_cursor])
                a_cursor += 1
            elif b[b_cursor] < a[a_cursor] and (a_cursor != len(a) and b_cursor != len(b)):
                output_array.append(b[b_cursor])
                b_cursor += 1
        output_array += (a[a_cursor:] if a_cursor < len(a) else (b[b_cursor:] if b_cursor < len(b) else []))


        return output_array


    return merge(merge_sort(x[:len(x)//2]), 
                 merge_sort(x[len(x)//2:]))





    

if __name__ == '__main__':
    print(merge_sort([321312, 35435, 543213, 2132143, 5436543, 2]))