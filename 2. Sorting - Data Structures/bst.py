
# simple implementation of a binary search tree


class Node:
    def __init__(self, x: int):
        self.val = x
        self.r = self.l = None
    

    # insertion inserts a new value given a specific node
    def _insert(self, x: int):
        if x > self.val:
            if self.r is not None: self.r._insert(x)
            else: self.r = Node(x)
        else:
            if self.l is not None: self.l._insert(x)
            else: self.l = Node(x)    


    # returns the elements of a BST in the order of magnitude
    def _inorder_traversal(self) -> list:
        if self.l is self.r is None:
            return [self.val]
        left = [] if self.l is None else self.l._inorder_traversal()
        right = [] if self.r is None else self.r._inorder_traversal()


        return left + [self.val] + right

    # returns the rightmost leaf of a specific node
    def _rightmost_leaf(self) -> Node:
        if self.r is None: return self
        else:
            return self.r._rightmost_leaf()
    # returns the leftmost leaf of a specific node
    def _leftmost_leaf(self) -> Node:
        if self.l is None: return self
        else:
            return self.l._leftmost_leaf()



    # finds the in-order successor of a specific node
    def _inorder_successor(self) -> Node:
        if self.r is not None: return self.r._leftmost_leaf()
        else:
            return None
    
