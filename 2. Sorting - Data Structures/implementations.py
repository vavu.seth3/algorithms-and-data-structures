import heap
import bst



# based on the implementation of min-heap within heap.py
def heap_sort(x: list) -> list:
    x = heap.min_heapify(x)
    

    sorted_list = []
    while len(x) != 0:
        x, e = heap.pop_min(x)
        sorted_list.append(e)

    return sorted_list


# implementation of BST sort
def bst_sort(x: list) -> list:
    n = bst.Node(x[0])

    for k in x[1:]:
        n._insert(k)
    return n._inorder_traversal()




if __name__ == '__main__':
    print(bst_sort([5, 4, 3, 2, 1, 10, 30]))