# implementation of methods that maintain the heap property on a given list x
# follows the conventional structure of a heap
import math




# function that fixes a heap from the top down
def repair_heap(x: list, i: int) -> list:
    ltree = 2*i + 1
    rtree = 2*i + 2
    if ltree >= len(x) or rtree >= len(x) or (x[i] < x[ltree] and x[i] < x[rtree]): return x
    
    replacement_index = ltree if x[ltree] < x[rtree] else rtree
    x[replacement_index], x[i] = x[i], x[replacement_index]
    return repair_heap(x, replacement_index)




# given a list x, returns another list that represents a min heap
def min_heapify(x: list) -> list:
    leaf_begin = (len(x) // 2) - 1

    for k in range(leaf_begin, -1, -1):
        x = repair_heap(x, k)

    return x



# pops the minimum element from a heap
def pop_min(x: list) -> (list, int):
    x[0], x[-1] = x[-1], x[0]
    popped_elm = x[-1]
    del x[-1]
    
    if len(x) is not 0: x = min_heapify(x)

    return (x, popped_elm) 