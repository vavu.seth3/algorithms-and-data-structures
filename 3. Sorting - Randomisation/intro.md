# Randomized Algorithms

I'd like to motivate this section my looking at a sorting algorithm (yes another one). This sorting algorithm is unique in the fact that its worst-case complexity is significantly worse than some of the previously studied algorithms yet in the real world it performs 2 or 3 times faster than regular algorithms. The algorithm is known as **quick sort** and will serve as an introduction to randomised algorithms. <br/><br/>

### Quick sort
The algorithm is a rather simple one and is as follows:
>1. Chose an element of the array $A_{i}$, this element is known as the pivot.
>2. Generate two sub-arrays, one full of elements that are less than or equal to $A_{i}$ and one with elements greater.
>3. Quick sort the sub-arrays and merge them with $A_{i}$.

Note that the pivot element can be whatever we want (usually the last element). A pseudo-python implementation is as follows:
```py
def quick_sort(x: list) -> list:
  if len(x) == 0 or len(x) == 1: return x

  pivot = x[-1]
  left, right = [], []

  for k in x[:-1]:
    if k <= pivot: 
      left.append(k)
    else: 
      right.append(k)
  
  return quick_sort(left) + [pivot] + quick_sort(right)
```
What is important about this algorithm however is the analysis, we will perform a worst-case and average-case analysis before properly introducing the notion of randomisation.

##### Worst Case Complexity
Using a RAM model of computation, at most the algorithm makes $\binom{n}{2}$ comparisons, thus we can conclude: <br/>
$\begin{aligned}
    T_{n} \leq \binom{n}{2} \\
    \therefore T_{n} \leq \frac{n(n-1)}{2} \\ \\
    \therefore T_{n} = O(n^{2}) 
\end{aligned}$

Now this is obviously significantly worse than say merge-sort or heap sort but the power of quick sort isn't in its best case complexity but instead its average case complexity which we will analyse as follows:



##### Average Case Complexity
Consider an arbitrary input $A_{n}$, we define a random variable $X$ as the number of comparisons performed in an arbitrary execution of the algorithm, the goal is to compute the expected value of the random variable $E\left[X\right]$, we then define the average case complexity $T_{n}$ as the expected value. 

Consider an input of size $n$. Say that an element is chosen to be the pivot, this element when sorted could be the "center" of the sorted array or the very end of the sorted array, if we assume that all possible inputs are equally likely then we can compute $T_{n}$ as follows: <br/>
$\begin{aligned}
    T_{n} = E\left[X\right] = (n - 1) + \frac{1}{n} \sum_{i=0}^{n-1} \left(T_{i - 1} + T_{n - i}\right)
\end{aligned}$

The reasoning is as follows: Given an arbitrary pivot we will need to make $n-1$ comparisons in order to determine our new array partitions, the size of these partitions however are functions of the pivot we chose, each element in the "final sorted array" has a $\frac{1}{n}$ chance of being the pivot likewise sizes of the two generated sub arrays will depend on the chosen pivot. <br/>

$\begin{aligned}
    \text{Consider: } S_{n} = \frac{1}{n} \sum_{i=0}^{n-1} \left(T_i + T_{n - i}\right) \\ = \frac{2}{n} \sum_{i=1}^{n-1} T(i) \\
    \therefore T_{n} = (n-1) + \frac{2}{n} \sum_{i=1}^{n-1} T(i) \\ \\ \therefore T_{n} \leq (n - 1) + \frac{2}{n}\int_{1}^{n} T(x) \space dx
\end{aligned}$

This is a rather odd recurrence relation and we can't directly apply the master theorem to arrive at a solution, instead its easier to guess a solution and show that it satisfies the relation. We will guess that $T_{n} = O(n log(n))$ (How convenient).

$\begin{aligned}
    \text{Consider: } T_{n} = O(n \cdot log(n)) \\ \\
    (n - 1) + \frac{2}{n}\int_{1}^{n} c(n \cdot log(n)) \space dx = \cdots + \frac{cn}{2}\left(\frac{n^{2}}{2}\cdot ln(n) - \frac{n^{2}}{4} + \frac{1}{4}\right) \\ \leq cn\cdot log(n), \text{for c greater than  2} \\ \\
    \therefore T_{n} = O(n\cdot log(n)) \text{Satisfies the relation}
\end{aligned}$

Thus the average case performance of quick sort is $O(n\cdot log(n))$ as an added bonus, when tested experimentally quicksort on average performs 2-3 times faster than merge-sort (when implemented well). Note however that when performing this average case analysis we made a very crucial assumption **All arrangements of an n-sized array are equally likely**. This however is never really the case as a clever nuisance can intentionally feed in an array that guarantees worst-case performance. To work around this, we introduce the idea of randomisation. <br/><br/>


### Randomisation
When we analyse the average case performance of algorithms we usually perform a probabilistic analysis based on the assumption that all possibilities have the same likelihood. As shown above however, this is not always the case **BUT** we can make it the case ;). This is where randomisation comes in, the core idea behind randomised algorithms is introducing a random number generator along some step of our computation, this allows us to guarantee that for basically any input we are more likely to see the average case performance rather than the worst case performance. 

Sometimes randomisation may be introduced by shuffling the input or introducing some sort of bias but in the case of quicksort we will introduce randomness in our selection of the pivot as this is the most appropriate location. 

##### Randomised quicksort
Reasonably simple algorithm, we simply just employ a random number generator to select our pivot. A pseudo-python implementation is provided below. 
```py
def quick_sort(x: list) -> list:
  if len(x) == 0 or len(x) == 1: return x

  i, pivot = random.randint(0, len(x) - 1), x[i]
  left, right = [], []

  for k in x[:i] + x[i+1:]:
    if k <= pivot: 
      left.append(k)
    else: 
      right.append(k)
  
  return quick_sort(left) + [pivot] + quick_sort(right)
```

This introduction of randomisation now somewhat ensures that we can expect our quicksort algorithm to run in $nlog(n)$ time under a RAM model of computation. 

##### Types of randomisation
There are 2 main types of randomisation algorithms these are: Monte Carlo Algorithms (Always fast and maybe correct) and Las Vegas algorithms (sometimes fast but always correct). Quicksort is an example of a Las Vegas algorithm. 

A classic example of a monte carlo algorithm is the approximation of pi. The idea behind the algorithm is that we repeatedly sample points in a unit-square using their distance from the origin we can approximate pi.