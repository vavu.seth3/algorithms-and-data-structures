# Case Analysis

When studying the performance of an algorithm there are typically look at three main cases: worst, average, best and we study how the algorithm scales with each of these cases, essentially: we study its asymptotic behavior with each of these cases. Now the question is.... What does all this mean?
 - **Worst Case:** This is essentially a study of how the algorithm scales (with input size) when we feed it the WORST possible input we can.
 -  **Average Case:** This refers to how the running time scales on a random input, hence it is basically the expected value of the running time. The big assumption with average case analysis however is that we expect all inputs to be equally probable and we question how well this "average case" scales with input size. While average case is usually more indicative of the real world, sometimes malicious users inject intentionally worst case data; we can usually overcome this problem however with a degree of randomisation in our algorithms.
 - **Best Case:** This is in essence a measure of how our algorithm scales when we feed it the BEST possible input that we could give it, this is always a poor indicator of an algorithm's performance. Further note that algorithmic analysis is always performed on a specific model of computation, for the purpose of these notes all analysis is performed on the RAM model.

### Performing Case Analysis
Now that we've defined case analysis the important thing is to now figure out how we are going to perform case analysis. Typically worst and base case analysis is the easiest as it involves analysing only a single type of input while average case is more tricky as it involves computing an expected value. 

To motivate this section we will be studying a simple algorithm to sort a "list" of numbers, this algorithm known as "bogosort" randomly shuffles our list and checks if it is in sorted order. 

 - **Worst Case:** When performing a worst-case analysis we typically think about "what is the worst possible situation our algorithm can find itself in?" Well this is a rather simple question in the case of bogosort, our algorithm always generates an incorrect permutation. We define a function $f(n)$ which represents the operational time of our algorithm in the worst case, from the argument above it is clear to see that $f(n) \in O(\infty)$
 <br/>
 
 - **Best Case:** Best case analysis is just as simple as worst case analysis, in the worst case our algorithm never terminates but in the best case the first permutation it generates is the "sorted" permutation. However, we still need to confirm that this is our sorted partition and to do this we must inspect every element in our list. Assuming that "checking" and "comparing" index values takes a constant time $c_{0}$, we arrive at the following series: <br/>
 $\begin{aligned}
    \sum_{i = 1}^{n} c_{0} &= c_{0} \sum_{i = 1}^{n} \\
    &=c_{0}n \\
    &= O(n)
 \end{aligned}$ <br/>
 Hence in the best case the complexity is $O(1) + O(n) = O(n)$ 
 <br/>

 - **Average Case:** I've left this to last due to the innate difficulty of performing average case analysis. This analysis will be a bit more excessive than normal average case analysis but its for the sake of education :). <br/>
 Before the analysis I would like to introduce a type of random variable known as an "indicator random variable". An indicator random variable has a value of $1$ if a specific random event occurs and $0$ otherwise. Eg. We can define an indicator random variable $X_{i} = I\{\text{It rains today}\}$ to represent the event that it rains today. Now this may seem stupid but they're value becomes evident when computing expected values. Consider the expected value of some indicator random variable bounded to an event $A$: $X_{i} = I\{A\}$. <br/>
 $\begin{aligned}
    E[X_{i}] &= \sum^{1}_{i = 0} P(X_{i} = i)\\
    &= 0 \cdot (1 - P(A)) + P(1) \\
    &= P(A)
 \end{aligned}$<br/>
 Now hopefully the power of indicator random variables become evident, their expected value is simply just the probability of the event occurring, this holds great power when performing average case analysis. We do not need it here though.<br/>

 There are $n!$ permutations of the list only one of which is actually sorted, as such the expected number of "generations" that we must create are $n!$, since each of these have to be checked we can conclude that we make at most $n\cdot n!$ memory accesses and by extension the average case is $O((n + 1)!)$ 
 <br/>

### Some thoughts
From the above analysis of bogosort hopefully it is obvious why we perform all three types of analysis and don't just base our decisions on one type. This is because merely studying one analysis is never truly indicative of the real world. In the case of bogosort, we will generally tend to arrive at a sorted permutation before the heat death of the universe :O. 
<br/>

### Theorems
 - ###### The Master Theorem
    Sometimes we study algorithms whose complexities can be written as recursive formulas, for example the complexity for "merge sort" can be written as: <br/> 
    $\begin{aligned}
        T(n) = 2T\left(\frac{n}{2}\right) + c_{0}n
    \end{aligned}$ <br/>
    The master theorem provides a general set of solutions to reccurance relations of the form: <br/>
    $\begin{aligned}
        T(n) = a_{o}T\left(\frac{n}{b_{o}}\right) + cn^{k}
    \end{aligned}$<br/>
    The theorem is rather simple and all the proofs are left as an "exercise". <br/>
    $\begin{aligned}
        T(n) &\in \Theta(n^{k}), \text{  if  } a < b^{k} \\
        T(n) &\in \Theta(n^{k}\cdot \log{n}), \text{  if  } a = b^{k} \\
        T(n) &\in \Theta(n^{\log_{b}{a}}), \text{ if } a > b^{k}
    \end{aligned}$ <br/>
    There is a more general version of the master theorem but that as well as its proof can be found online.

 - ###### Average case of Bogosort
    When analysing bogosort I made the argument that since there were $n!$ permutations, the expected number of generations was $n!$, while this makes intuitive sense as n tends towards infinity, I would still like to outline the proof as to why this is the case. 
    <br/>
    Consider a general event with a probability $p$ of occurring, likewise we define the random variable $X$ to be the number of times we must repeat an experiment to witness the even occur for the first time. We can model the probability that $X = n$ using a geometric distribution of the form: <br/>
    $\begin{aligned}
        P(X = n) = (1 - p)^{n - 1}p
    \end{aligned}$<br/>
    We can compute the expected value of $X$ as follows: <br/>
    $\begin{aligned}
        E[X] &= \sum^{\infty}_{n = 1} np(1 - p)^{n - 1} \\\\
        &= p\sum^{\infty}_{n = 1} n(1 - p)^{n - 1}, \text{ since } |1-p| < 1, \text{ it follows: } \\\\
        &= p\sum^{\infty}_{n = 1} \frac{d}{dq}q^{n} \\\\
        &= p\cdot \frac{d}{dx}\left(\sum^{\infty}_{n = 1} q^{n}\right) \\
        &= p\cdot \frac{d}{dq}\left(\frac{1}{1 - q} - 1\right) \\ 
        &= \frac{p}{(1 - q)^{2}} = \frac{1}{p}
    \end{aligned}$<br/>
    Thus it is proven that if there were $n!$ permutations with only ONE being valid, then the expected number of shuffles was $n!$.


