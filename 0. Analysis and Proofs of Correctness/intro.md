## Basics of Algorithms

As discussed in the earlier sections, algorithms are in essence a description of some turing machine that performs a task, as such we need to be able to analyse algorithms in order to determine how well they perform.

Algorithmic analysis refers to analysis of how the running time of algorithms "grow" as our input size varies, there are 3 primary types of analysis: worst case, average case and best case. Now... It's hard to define a function that computes how long a program takes without running it, so we employ a little mathematical trick: asymptotic notation, asymptotic notation captures the limiting behavior function grows as opposed to the function itself. This chapter will be dedicated to introducing various types of asymptotic notation and orders of growth.

## 1. Big-O
Consider a function $f(n)$. We say that a function $f(n) \in O\left(g(n)\right)$ iff: <br/>
$|f(n)| \leq C |g(n)| \text{  for all  } n > k$<br/>
The reason why we are only concerned with n that exceed a value k is because the point of "Big O" is the capture the asymptotic behavior of a function, not the "general behavior". Big O intuitively gives us an upper bound on the asymptotic behavior of a function, in essence it is saying: as n tends to infinity, the function never tends above g(n).

##### Properties of Big O
 - Consider a function: $f(x) = a_n x^{n} + a_{n-1}x^{n-1} + \cdots + a_{1}x + a_{0}$, I claim that $f(x) \in O(x^{n})$. The proof is outlined below. <br/>
 $\begin{aligned}
    |f(x)| &= |a_n x^{n} + a_{n-1}x^{n-1} + \cdots + a_{1}x + a_{0}| \\
    &= |a_n| x^{n} + |a_{n-1}|x^{n-1} + \cdots + |a_{1}|x + |a_{0}| \\
    &\leq x^{n}\left(|a_{n}| + |a_{n - 1}| + \cdots + |a_{1}| + |a_{0}|\right) \\
    &= c|x^{n}| \\ \\ \therefore f(x) \in O(x^{n})
 \end{aligned}$
 <br/>
 
 - If $f_{1}(x)$ and $f_{2}(x)$ $\in O\left(g(x)\right)$ then $f(x) = f_{1}(x) + f_{2}(x) \in O(g(x))$. Once again the proof is simple and is outlined below <br/>
 $\begin{aligned}
    |f(x)| &= |f_{1}(x) + f_{2}(x)| \\
    &\leq c_{1}|g(x)| + c_{2}|g(x)| \\
    &= (c_{1} + c_{2})|g(x)| \\ \\ \therefore f(x) \in O(g(x))
 \end{aligned}$
 <br/>

 - If $f_{1}(x) \in O(g(x))$ and $f_{2} \in O(h(x))$ then $f(x) = f_{1}(x) + f_{2}(x) \in O(max(|h(x)|, |g(x)|))$. The proof is outlined below <br/>
 $\begin{aligned}
    |f(x)| &= |f_{1}(x) + f_{2}(x)| \\
    &\leq c_{1}|g(x)| + c_{2}|h(x)| \\
    &\leq (c_{1} + c_{2})max(|h(x)|, |g(x)|) \\ \\ \therefore f(x) \in O(max(|h(x)|, |g(x)|))
 \end{aligned}$

All these properties make it super easy to compute the asymptotic upper bounds. I'll cover some examples of computing asymptotic upper bounds a bit later.

## 2. Big-$\Omega$
Like Big-O, consider a function $f(x)$. We say a function $f(x) \in \Omega(g(x))$ iff: <br/>
$|f(x)| \geq C |g(x)| \text{  for all  } x > k$<br/>
Intuitively, like Big-O, Big-Omega measures the asymptotic growth of a function except it puts a "lower bound" on it, it claims that as the function grows it NEVER goes beneath a certain function.

##### Properties of Big Omega
 - $f(x)$ is $O(g(x))$ iff $g(x)$ is $\Omega(f(x))$ This theorem is rather obvious and doesn't really require a proof.

## 3. Big-$\Theta$
Consider a function $f(x)$, we say a function $f(x) \in \Theta(g(x))$ iff $f(x) \in O(g(x))$ and $f(x) \in \Omega(g(x))$. If a function is $\Omega(g(x))$ we say it is of order $g(x)$. Intuitively it means that f(x) is asymptotically tightly bounded by g(x).

##### Properties of Big $\Theta$
 - Consider a polynomial $f(x) = a_n x^{n} + a_{n-1}x^{n-1} + \cdots + a_{1}x + a_{0}$ where $a_{n} > 0$. $f(x) \in \Theta(x^{n})$. Proof is left as an exercise to the reader.
 - If we have two functions $f_{1}(x) \in \Theta(g_{1}(x))$ and $f_{2}(x) \in \Theta(g_{2}(x))$ and $g_{2}(x) \in O(g_{1}(x))$ then $f(x) + g(x) \in \Theta(g(x))$.
<br/>
<br/>


## Asymptotic Proofs
 1. **Proof that $\log{n!} = \Theta(n\log{n})$**
    By Stirling's approximation: <br/>
    $\begin{aligned}
        &n! = \sqrt{2\pi n}\left(\frac{n}{e}\right)^{n}\left(1 + \Theta\left(\frac{1}{n}\right)\right) \\
        \implies \log{n!} &= \log{\left(\sqrt{2\pi n}\left(\frac{n}{e}\right)^{n}\left(1 + \Theta\left(\frac{1}{n}\right)\right)\right)}\\
        &= \log{\sqrt{2\pi n}\left(\frac{n}{e}\right)^{n}} + \log{\left(1 + \Theta\left(\frac{1}{n}\right)\right)} \\
        &\approx \log{\sqrt{2\pi}} + \frac{1}{2}\sqrt{n} + n\log{n} - n\log{e} \\
        &= \Theta(1) + \Theta(\log{n}) + \Theta(n\log{n}) - \Theta(n) \\
        &= \Theta(n\log{n})
    \end{aligned}$<br/>