# **Preliminary Overview** #

The field of algorithms and data structures is fundamentally about an informal and cursory description of solving a problem given a sequence of steps. We utilise data structures to structure/order data while algorithms perform processes on such data.

Personally for me however, such a definition seems rather odd and somewhat "un-satisfying", the rest of this section is dedicated to attempting to formally/intuitively describe what algorithms are, how they relate to the theory of computation **(my favorite discipline in theoretical CS)**. <br><br><br>
## **Programs, machines and finite automata** ##
A question that I like to think about a lot is what exactly a program is **(outside of the hand-wavey "a sequence of steps")**, although I have no idea if my thoughts to follow are formally, after a preliminary introduction and study of the theory of computation, I believe I have a somewhat more "intuitive description" of a program.

Both deterministic and non-deterministic finite automata recognize regular languages which may also be generated via regular expressions, for example the regular expression: `R = 0*10*10*(10*|null)` describes the following language:   

$\ell = \{\omega \text{ | } \omega \text{ has 2 or 3 occurrences of 1 where the first two occurrences are not consecutive}\}$ 

Note that since the language is regular, we can also draw up a corresponding finite automata that is capable of "recognising" the language. For example the NFA that recognises the language described by the regular expression is as follows: ![](resources/NFA.png) As such, I like to think of regular expressions as a sort of "blueprint" for constructing a finite automata, it a description of what the finite automata that recognises the language should look like and it is up to a human to perform this conversion. As such, regular expression can be thought of as some sort of "program" for us humans, we read this program and create a finite automata that can recognise the language. 

I draw this analogy to a modern day computer, a program in essence is a description of a sort of description of a machine that we want the computer (**a universal turing machine**) to simulate. In that sense, programs define and describe machines and the general purpose computer simulates the machine as described by a program.
<details>
    <summary><b>Universal Turing Machines</b></summary>
    A turing machine is in essence a mathematical tool that is equivalent to a general purpose computer, it is able to recognise recursively-enumerable languages. A universal turing machine however, is a machine that can simulate any turing machine given a description of the machine, this description is the sort of "program" for the universal turing machine. </br> Note how sometimes we talk about the turing-completeness of languages, in essence that means that the language can be used to construct a universal turing machine which can simulate any other turing machine, in essence the language can reproduce a turing machine for any given "program". 
</details> </br>
This gives us a somewhat reasonable understanding of what a program is, it is in essence, a description of a machine, the machine is then simulated by our general purpose computer. Honestly this all makes the field of computer science seem like such a wonderful discipline.
<br><br>

## Models of computation ##
Before introducing and studying the notion of an algorithm, I first want to clarify the idea of a "model of computation". Much like how Maxwell's 2nd equation: $\nabla\cdot B = 0$ is in essence a mathematical model of some aspect of the magnetic field, computational models provide computer scientists with a model/description of how real world computers behave. These models of computation define sets of permissible instructions/methods that we can utilise particularly in our description of other machines, for example a push-down automata permits: pushing, popping and reading. Every program we right is restricted to a certain model of computation and by extension so are our algorithms. https://computationstructures.org/notes/models/notes.html <br><br>

## Algorithms (what are they?) ##

Algorithms are essentially an informal description of steps that we utilise to solve a specific computational problem, whether this be ordering a list such that each successive element is bigger than the next one or finding the shortest path in a map between two points. The inherently hand-wavy nature of an algorithm implies that when we attempt to implement an algorithm as a program, we are restricted to the instructions provided to us by the model of computation. Thus when we analyse an algorithm, we always analyse such algorithms under a certain model of computation. **The most common models of computation are the RAM machine and the pointer machine.**